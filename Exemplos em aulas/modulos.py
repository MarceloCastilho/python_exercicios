import math
import random
import emoji

## MATH ##
#ceil = arrendonda para cima
#floor = arredonda para baixo
#sqrt = raiz quadrada


num = int(input("Digite um numero: "))
raiz = math.sqrt(num)
print(f"A raiz de {num} é: {math.floor(raiz)}")

## RANDOM ##

#random.random() = numero aleatório infinito
#random.randint(x, x)= numero aleatório inteiro entre 2 numeros

print(random.random())
print(random.randint(1, 100))

## Emoji ##
#https://www.webfx.com/tools/emoji-cheat-sheet/

print(emoji.emojize("Olá Mundo :earth_americas:", language='alias'))