frase = 'Curso em Vídeo Python'

print(frase[0:5])
print(frase[9:14])
print(frase[15:])
print(frase[::2])

# EM python tudo é tratado como objeto, consigo contar, aumentar as letras e tudo mais
print(frase.count('o'))
print(frase.upper().count('O'))

# Len conta a tamanho da STR
print(len(frase))

# Posso trocar as palavras, com o replace
print(frase.replace('Python', 'Spark'))


print('Curso' in frase) # Descubro se a palavra Curso existe na frase
print(frase.find('Python')) # Descubro a posição

dividido = frase.split()
print(dividido[0] [2])