# Desenvolva um programa que leia o comprimento de três retas
#  e diga ao usuário se elas podem ou não formar um triângulo

lado1 = float(input('Digite o tamanho da primeira reta: '))
lado2 = float(input('Digite o tamanho da segunda reta: '))
lado3 = float(input('Digite o tamanho da terceira reta: '))

if (lado1 + lado2 < lado3) or (lado1 + lado3 < lado2) or (lado2 + lado3 < lado1):
    print('Não forma um triangulo')
else:print('Forma um triangulo')