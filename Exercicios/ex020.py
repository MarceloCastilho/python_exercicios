# O mesmo professor do desafio anterior
#  quer sortear a ordem de apresentação de trabalho dos alunos. 
# Faça um programa que leia o nome dos quatro alunos e mostre a ordem sorteada

from random import sample

alunos = []

for i in range(4):
    alunos.append(input("Qual seu nome? "))
    
sorteado = sample(alunos, k=len(alunos))
enumerar = list(enumerate(sorteado))
print(*enumerar, sep='\n')
