# Faça um programa que leia um número inteiro e mostre na tela o seu sucessor e seu antecessor

n = int(input("Digite um número: "))
print(f"""
Seu número é {n}
Seu sucessor é {n+1}
Seu antecessor é {n-1}
""")

