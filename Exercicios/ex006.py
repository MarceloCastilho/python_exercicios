#Crie um algoritmo que leia um número e mostre o seu dobro, triplo e raiz quaadrada

n = int(input("Digite um número: "))

print(f""""
Seu número: {n}
Seu dobro: {n*2}
Seu triplo: {n*3}
Sua raíz quadrada: {n**0.5}
""")