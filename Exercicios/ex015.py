# Escreva um programa que pergunte a quantidade de KM percorridos por um carro alugado
#  e a quantidade de dias pelos quais ele foi alugado.
#  Calcule o preço a pagar, sabendo que o carro custa R$60 por dia e R$0.15 por km rodado

dias = int(input("Por quantos dias o carro foi alugado: "))
km_rodado = float(input("Quantos KMs ele foi rodado: "))

valor_dias = dias * 60
valor_km_rodado = km_rodado * 0.15
valor_total = valor_dias + valor_km_rodado

print(f"O valor total é de: R${valor_total}")