### Crie um programa que leia o nome completo de uma pessoa e mostre:
# 1. O nome com todas as letras maiúsculas
# 2. O nome com todas minúsculas
# 3. Quantas letras ao todo (sem considerar espaços)
# 4. Quantas letras tem o primeiro nome

nome = input("Digite seu nome completo: ")
print('\n ')

nomeMaiusculo = nome.upper()
print('Nome Completo em maiúsculo: ', nomeMaiusculo)

nomeMinusculo = nome.lower()
print('Nome Completo em minúsculo: ', nomeMinusculo)

qtd_letras = len(nome.replace(" ", ""))
print('Quantidade de letras ao todo (sem considerar espaços): ', qtd_letras)

qtd_letras_1nome = nome.split()
print('Quantas letras tem o primeiro nome: ', len(qtd_letras_1nome[0]))