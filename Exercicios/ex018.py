# Faça um programa que leia um angulo qualquer e mostre na tela o valor do seno,
# cosseno e tangente desse angulo
import math

angulo = float(input("Diga o angulo: "))

print(f"""
Seno = {round(math.sin(math.radians(angulo)), 2)}
Cosseno = {round(math.cos(math.radians(angulo)), 2)}
Tangente = {round(math.tan(math.radians(angulo)), 2)}
""")