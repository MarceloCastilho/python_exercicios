# Escreva um programa que faça o computador "Pensar" em um número inteiro entre 0 e 5 
# e peça para o usuário tentar descobir qual foi escolhido pelo computador
# O programa deverá escrever na tela se o usuário venceu ou perdeu
import random

def advinha_num():
    num_aleatorio = random.randint(0,5)
    resp = int(input('Advinhe o número de 0 a 5 que estou pensando: '))
    if num_aleatorio == resp:
        print(f'CAGADO, era {resp} mesmo')
    else: print(f'ERRRRROU, era {num_aleatorio}')

advinha_num()