#Escreva um programa que leia um valor em metros 
# e o exiba convertido em centimetros e milimetros

n = int(input("Digite um valor em metros: "))
c = n * 100
m = n * 1000

print(f"""
Valor digitado em metros: {n}
Em centimetros: {c}
Em milimetros: {m}
""")