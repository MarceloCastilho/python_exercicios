# Faça um programa que leia uma frase pelo teclado e mostre:
# Quantas vezes a letra "A" aparece
# Em que posição ela aparece a primeira vez
# Em que posição ela aparece a ultima vez

frase = input("Digite uma frase: ").upper()

print(f"A letra 'A' aparece {frase.count('A')} vezes")

print(f"A letra 'A', aparece pela primeira vez na {frase.find('A')} posição")

print(f"A letra 'A', aparece pela ultiva vez na {frase.rfind('A')} posição")