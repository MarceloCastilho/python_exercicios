# Faça um programa que leia algo pelo teclado 
# e mostre na tela o seu tipo primitivo e todas as informações sobre ele

x = input("Digite algo: ")
print(f"O tipo primitivo desse valor é {type(x)}")
print(f"Só tem espaços? {x.isspace()}")
print(f"É um número? {x.isnumeric()}")
print(f"É alfabético? {x.isalpha()}")
print(f"Está em maiúscula? {x.isupper()}")
print(f"Está em minúscula? {x.islower()}")
print(f"Está capitalizada? {x.istitle()}")