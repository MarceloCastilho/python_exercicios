# Faça um programa que leia um nome completo de uma pessoa,
# mostrando em seguida o primeiro e o último nome separadamente
# ex: Ana Maria de Souza
#    primeiro: Ana
#    ultimo: Souza

nome = input("Digite seu nome completo: ").split()

print(f"""

Nome completo: {' '.join(nome)}
Primeiro nome: {nome[0]}
Último Nome: {nome[-1]}
""")