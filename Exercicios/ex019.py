# Um professor quer sortear um dos seus quatro alunos para apagar o quadro.
# Faça um programa que ajude ele, lendo o nome deles e escrevendo o nome do escolhido
import random

alunos = []

for i in range(4):
    alunos.append(input("Qual seu nome? "))

print(f"O aluno(a) escolhido é: {random.choice(alunos)}")



