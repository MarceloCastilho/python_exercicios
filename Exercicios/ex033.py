# Faça um programa que leia três números e mostre qual é o maior e qual é o menor

num = []

for i in range(3):
    num.append(float(input('Digite um número: ')))

print(f'O maior número é {max(num)} e o menor é {min(num)}')