# Faça um programa que leia um número de 0 a 9999 
# e mostre na tela cada um dos dígitos separados
# EX: 1894
# Unidade: 4 
# Dezena:3 
# Centena: 8 
# Milhar: 1


def dividirnum():
        num = int(input("Digite um número menor ou igual a 9999: "))
        if num <= 9999 and num > 999:
            numL= str(num)
            print(f"""
            Resposta: 
Unidade: {numL[3]}
Dezena: {numL[2]}
Centena: {numL[1]}
Milhar: {numL[0]}
""")
        elif num <= 999 and num > 99:
            numL= str(num)
            print(f"""
            Resposta: 
Unidade: {numL[2]}
Dezena: {numL[1]}
Centena: {numL[0]}
""")
        elif num <= 99 and num > 9:
            numL= str(num)
            print(f"""
            Resposta: 
Unidade: {numL[1]}
Dezena: {numL[0]}
""")
        elif num >= 9 and num >= 0:
            numL= str(num)
            print(f"""
            Resposta: 
Unidade: {numL[0]}
""")
        else:
            print("Apenas numeros positivos menores que 9999 ", '\n')
            dividirnum()

dividirnum()
