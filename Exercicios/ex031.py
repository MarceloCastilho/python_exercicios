# Desenvolva um programa que pergunte a distância de uma viagem em KM.
# Calcule o preço da passagem, cobrando R$0.50 por KM rodado
# para viagens de até 200KM e R$0.45 para viagens mais longas

km = float(input('Digite a distancia da viagem em KM: '))

if km <= 200:
    print(f'O valor será de R${km * 0.5}')
else: print(f'O valor será de R${km * 0.45}')
