# Faça um algoritmo que leia o preço de um produto 
# e mostre seu novo preço, com 5% de desconto

n = float(input("Digite o valor do produto: "))

print(f"Com os descontos, o valor do produto será R${n - n*0.05}")