# FAça um programa que leia o comprimeto oposto e do cateto adjacente de um triangulo retangulo,
#  calcule e mostre o comprimento da hipotenusa

import math

cateto_oposto = float(input("Cateto Oposto = "))
cateto_adjacente = float(input("Cateto adjacente = "))
hipotenusa = math.sqrt(cateto_oposto*cateto_oposto + cateto_adjacente*cateto_adjacente)

print(f"A Hipotenusa é {round(hipotenusa, 2)}")